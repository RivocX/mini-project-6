# Week 6 Mini Project
> Puyang(Rivoc) Xu (px16)

This project is a Rust Lambda Function with Logging and Tracing. I reuse my Mini Project2. This function takes a JSON request containing an integer, calculates the square of that integer, and returns the result in JSON format. Logging and Tracing functions are added.

## Detailed steps
### Preparation
Create a new role on AWS IAM. Make sure they have following permissions policies: 
* AmazonDynamoDBFullAccess
* AWSLambda_FullAccess
* AWSLambdaBasicExecutionRole
* AWSXRayDaemonWriteAccess
* IAMFullAccess

### Modify the project
In `/src/main.rs`, add the following library.
```
use tracing_subscriber::FmtSubscriber;
use tracing::{info, Level};
```
And add logging codes in main function.
```
// Logging
let subscriber = FmtSubscriber::builder().with_max_level(Level::INFO).finish();
tracing::subscriber::set_global_default(subscriber).expect("Failed to set up global default logger");
info!("Initializing the service...");
```
Make sure you have related tracing dependencies in `Cargo.toml`.

### Deploy
Then build and deploy this project on AWS.
```
cargo lambda build
```
```
cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::339712859714:role/Week6
```

### Test
Create a new stage and a new resource. Use the given url for test. I use Postman for testing. You can also use `curl` command as following.
```
curl -X POST https://hhqftacd8d.execute-api.us-east-1.amazonaws.com/test/square -H "Content-Type: application/json" -d '{"body": "{\"number\": 10}"}'
```

### X-Ray tracing
Enable `X-Ray Active tracing` and `Lambda Insights Enhanced monitoring` in Lambda configuration. Then you can see logging and tracing in CloudWatch.



## Results & Screenshots
### Lambda function
![](./Lambda.png)

### Testing by Postman
![](./Postman.png)

### X-Ray
![](./X-Ray.png)

### CloudWatch
### Traces
![](./Trace-1.png)

![](./Trace-1.png)

### Logs
![](./Logs.png)
